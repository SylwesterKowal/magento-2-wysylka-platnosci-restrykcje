<?php


namespace Kowal\PolskiePlatnosciWysylka\Model\Payment;

class Gotowka extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "gotowka";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
        return parent::isAvailable($quote);
    }
}
