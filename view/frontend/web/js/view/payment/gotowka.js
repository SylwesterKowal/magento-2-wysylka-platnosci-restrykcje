define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'gotowka',
                component: 'Kowal_PolskiePlatnosciWysylka/js/view/payment/method-renderer/gotowka-method'
            }
        );
        return Component.extend({});
    }
);